# my-webpack-template

> Version 0.0.2: Added webpack-dev-server

## How to use:

* Prescribe the command `npm install` or `yarn`
* For development we prescribe the command `npm dev` or `yarn dev`
* For production we prescribe the command `npm build` or `yarn build`
* Profit!

![Meow)](https://thumbs.dreamstime.com/z/cat-lying-floor-37057229.jpg)